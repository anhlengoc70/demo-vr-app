import React, { useEffect } from 'react'
import queryString from 'query-string'
import { PageContainer } from '../theme/CommonStyle'
import { useHistory, useLocation } from 'react-router-dom'
import SceneManager from '../containers/booth/Booth'
const HomePage = () => {
  const location = useLocation()
  const history = useHistory()
  const search = queryString.parse(location.search)

  useEffect(() => {
    if (search?.from) {
      history.push({
        search: undefined
      })
    }
  }, [history, search.from])

  return (
    <PageContainer>
      <SceneManager />
    </PageContainer>
  )
}

export default HomePage
