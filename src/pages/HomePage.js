import React, { useEffect } from 'react'
import queryString from 'query-string'
import { PageContainer } from '../theme/CommonStyle'
import { useHistory, useLocation } from 'react-router-dom'
import HomeContainer from '../containers/home/Home'

const HomePage = () => {
  const location = useLocation()
  const history = useHistory()
  const search = queryString.parse(location.search)

  useEffect(() => {
    if (search?.from) {
      history.push({
        search: undefined
      })
    }
  }, [history, search.from])

  return (
    <PageContainer>
      <HomeContainer />
    </PageContainer>
  )
}

export default HomePage
