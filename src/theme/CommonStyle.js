import styled from 'styled-components'
import { themeGet } from '@styled-system/theme-get'
import {
  DownloadOutlined,
  HeartOutlined,
  MenuOutlined,
  EyeOutlined,
  HeartFilled,
  PlusOutlined,
  EditOutlined,
  DeleteOutlined
} from '@ant-design/icons'
import { Pagination } from 'antd'

const HeartIconHeader = styled(HeartOutlined)`
  color: ${themeGet('colors.highlight')};
  font-size: 1.3rem;
  margin: 0 2rem;
  cursor: pointer;
`
const HeartIcon = styled(HeartOutlined)`
  color: ${themeGet('colors.highlight')};
  font-size: 1.3rem;
  margin: 0 2rem;
  cursor: pointer;
  // position: absolute;
  // margin-right: 1rem
  transition: all 0.2s;

  &:hover {
    transform: scale(1.3);
    // margin: 1 !important;
    // width: 1rem !important;
    // position: absolute;
  }
`

const HeartFillIcon = styled(HeartFilled)`
  color: ${themeGet('colors.highlight')};
  font-size: 1.3rem;
  margin: 0 2rem;
  cursor: pointer;

  &:hover {
    color: ${themeGet('colors.highlight')};
    transform: scale(1.3);
    // margin: 0.1 !important;
    // position: absolute;
    transition: all 0.2s;
  }
`

const MenuIcon = styled(MenuOutlined)`
  color: #777;
  font-size: 1.3rem;
  cursor: pointer;
  margin-left: 1rem;

  &:hover {
    color: ${themeGet('colors.primary')};
  }
`

const DownloadIcon = styled(DownloadOutlined)`
  color: #297ad2;
  font-size: 1.3rem;

  /* &:hover {
    color: ${themeGet('colors.primary')};
  } */
`

const EyeIcon = styled(EyeOutlined)`
  color: ${themeGet('colors.primary')};
  font-size: 1.3rem;

  /* &:hover {
    color: ${themeGet('colors.primary')};
  } */
`

const PlusIcon = styled(PlusOutlined)`
  color: #777;
  font-size: 1.3rem;

  &:hover {
    color: ${themeGet('colors.primary')};
  }
`

const EditIcon = styled(EditOutlined)`
  color: #777;
  font-size: 1.3rem;

  &:hover {
    color: ${themeGet('colors.primary')};
  }
`

const DeleteIcon = styled(DeleteOutlined)`
  color: #777;
  font-size: 1.3rem;

  &:hover {
    color: ${themeGet('colors.primary')};
  }
`

const Container = styled.div`
  max-width: 100%;
  height: 100%;
  margin: auto;
  //padding: 0 1.5rem;

  @media (max-width: 1024px) {
    padding: 0 2rem;
  }

  @media (max-width: 768px) {
    padding: 0 1.5rem;
  }

  @media (max-width: 500px) {
    padding: 0;
  }
`

const PageWrapper = styled.div`
  display: flex;
  width: 100%;
  //min-height: ${themeGet('pageHeight')};
  //padding-top: 74px;
`

const PageContainer = styled.div`
  width: 100%;
  //padding: 3rem 0;

  @media (max-width: 500px) {
    padding: 0;
  }
`

const NewLabel = styled.p`
  position: absolute;
  bottom: 0;
  left: 0;
  background: ${themeGet('colors.highlight')};
  font-size: ${themeGet('fontSizes.5')};
  color: ${themeGet('colors.white')};
  line-height: 2.3rem;
  width: 70px;
  text-align: center;
  margin: 0;

  @media (max-width: 768px) {
    font-size: ${themeGet('fontSizes.3')};
    line-height: 2rem;
  }
`

const PaginationCustom = styled(Pagination)`
  .ant-pagination-item-active {
    border-color: ${themeGet('colors.primary')};

    a {
      color: ${themeGet('colors.primary')};
    }
  }

  .ant-pagination-item:hover,
  .ant-pagination-item-link:hover {
    border-color: ${themeGet('colors.primary')};

    a,
    svg {
      color: ${themeGet('colors.primary')};
    }
  }
`

export {
  HeartIcon,
  MenuIcon,
  DownloadIcon,
  Container,
  PageWrapper,
  NewLabel,
  PaginationCustom,
  PageContainer,
  EyeIcon,
  HeartFillIcon,
  PlusIcon,
  EditIcon,
  DeleteIcon,
  HeartIconHeader
}
