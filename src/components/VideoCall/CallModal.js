import React, { Component } from 'react'
// import VideocamIcon from "@material-ui/icons/Videocam";
// import CallIcon from '@material-ui/icons/Call'
// import MicIcon from '@material-ui/icons/Mic'
import bounce from 'react-animations'
import styled, { keyframes } from 'styled-components'
// import { Box, withStyles, Button } from '@material-ui/core'

const Bounce = styled.div`
  animation: 2s ${keyframes`${bounce}`} infinite;
`

const CallModal = () => {
  const classes = useStyles()
  // onClickAnswerCall =()=> {
  //   this.props.answerCall(false, true);
  // }
  // onClickRejectCall =()=> {
  //   this.props.rejectCall(true);
  // }
  return (
    <Box className={classes.callmodal}>
      <p>
        <span
          className={classes.caller}
        >{`${this.props.localID} is calling`}</span>
      </p>
      <Bounce>
        <Button
          type='button'
          className={classes.btnaction}
          onClick={this.onClickAnswerCall}
        >
          {/* <VideocamIcon></VideocamIcon> */}aaa
        </Button>
      </Bounce>
      <Bounce>
        <Button
          type='button'
          className={classes.btnaction}
          onClick={this.onClickAnswerCall}
        >
          {/* <MicIcon></MicIcon> */}bbb
        </Button>
      </Bounce>
      <Bounce>
        <Button
          type='button'
          className={classes.btnactionCancel}
          onClick={this.onClickRejectCall}
        >
          {/* <CallIcon></CallIcon> */}ccc
        </Button>
      </Bounce>
    </Box>
  )
}

const useStyles = withStyles((theme) => {
  return {
    callmodal: {
      position: 'absolute',
      width: '400px',
      padding: '20px',
      left: 'calc(50vw - 200px)',
      top: 'calc(50vh - 60px)',
      borderRadius: '5px',
      textAlign: 'center',
      backgroundColor: 'green',
      '&.active': {
        display: 'block',
        zIndex: '9999',
        animation: 'blinking 3s infinite linear'
      }
    },
    caller: {
      color: 'blue',
      fontSize: '1.5em'
    },
    btnaction: {
      height: '60px',
      width: '60px',
      margin: '20px 30px 0px 0px',
      lineHeight: '60px',
      textAlign: 'center',
      borderRadius: '50%',
      border: 'solid 2px $main-color',
      cursor: 'pointer',
      transitionDuration: '0.25s',
      backgroundColor: 'transparent',
      '&:hover': {
        backgroundColor: '#FFFFFF'
      }
    },
    btnactionCancel: {
      height: '60px',
      width: '60px',
      margin: '20px 30px 0px 0px',
      lineHeight: '60px',
      textAlign: 'center',
      borderRadius: '50%',
      border: 'solid 2px $main-color',
      cursor: 'pointer',
      transitionDuration: '0.25s',
      backgroundColor: 'transparent',
      '&:hover': {
        backgroundColor: 'red'
      }
    }
  }
})
export default CallModal
