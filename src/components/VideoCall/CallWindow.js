import React, { useEffect, createRef, useState } from 'react'
import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import CallIcon from '@material-ui/icons/Call'
// import VideocamIcon from "@material-ui/icons/Videocam";
import MicIcon from '@material-ui/icons/Mic'
import Peer from 'skyway-js'

import { makeStyles, Button } from '@material-ui/core'
import { RemoveRedEye } from '@material-ui/icons'

var dirLight, spotLight
let localStream = null
let remoteStream = null
var viewer = Math.PI.toString(36).substr(2, 9)
var peer
function CallWindow({ scene, streamer }) {
  const refLocalVideo = createRef()
  const refRemoteVideo = createRef()
  const [statusCall, setStatusCall] = useState('Wating for streamer answer ...')
  const classes = useStyles()

  useEffect(() => {
    init()
    console.log(refRemoteVideo.current)
    navigator.mediaDevices
      .getUserMedia({ video: true, audio: true })
      .then(function (mediaStream) {
        localStream = mediaStream
        refLocalVideo.current.srcObject = localStream

        peer = new Peer(viewer, {
          key: 'e55b0dee-e162-4f9d-9082-e3f045a0220b',
          debug: 3
        })
        peer.on('open', function (id) {
          if (streamer != '') {
            let conn = peer.connect(streamer)
            conn.on('open', function () {
              conn.on('data', function (data) {
                if (data === 'off') refRemoteVideo.current.srcObject = null
                if (data == 'on' && remoteStream)
                  refRemoteVideo.current.srcObject = remoteStream
              })
              // conn.send('Hello!');
            })

            setStatusCall('You are calling to : ' + streamer)
            var call = peer.call(streamer, localStream)
            call.on('stream', function (stream) {
              console.log(refRemoteVideo.current)
              console.log(stream)
              remoteStream = stream
              var video = document.getElementById('videoRemote')
              video.srcObject = remoteStream
              video.play()
              setStatusCall('You are talking to : ' + streamer)
            })
          }
        })
      })
  }, [peer])
  const init = () => {
    // spotLight = new THREE.SpotLight(0xffffff);
    // spotLight.name = "Spot Light";
    // spotLight.angle = Math.PI / 5;
    // spotLight.penumbra = 0.3;
    // spotLight.position.set(10, 10, 5);
    // spotLight.castShadow = true;
    // spotLight.shadow.camera.near = 8;
    // spotLight.shadow.camera.far = 30;
    // spotLight.shadow.mapSize.width = 1024;
    // spotLight.shadow.mapSize.height = 1024;
    // scene.add(spotLight);

    // dirLight = new THREE.DirectionalLight(0xffffff, 1);
    // dirLight.name = "Dir. Light";
    // dirLight.position.set(0, 10, 0);
    // dirLight.castShadow = true;
    // dirLight.shadow.camera.near = 1;
    // dirLight.shadow.camera.far = 10;
    // dirLight.shadow.camera.right = 15;
    // dirLight.shadow.camera.left = -15;
    // dirLight.shadow.camera.top = 15;
    // dirLight.shadow.camera.bottom = -15;
    // dirLight.shadow.mapSize.width = 1024;
    // dirLight.shadow.mapSize.height = 1024;
    // scene.add(dirLight);

    var textureLocal = new THREE.VideoTexture(refLocalVideo.current)
    var textureRemote = new THREE.VideoTexture(refRemoteVideo.current)

    var geometryLocal = new THREE.PlaneBufferGeometry(4, 3, 20, 15)
    var materialLocal = new THREE.MeshBasicMaterial({
      map: textureLocal,
      side: THREE.DoubleSide
    })
    var geometryRemote = new THREE.PlaneBufferGeometry(10, 7, 30, 20)
    var materialRemote = new THREE.MeshBasicMaterial({
      map: textureRemote,
      side: THREE.DoubleSide
    })

    var localPlay = new THREE.Mesh(geometryLocal, materialLocal)
    localPlay.position.set(5, -2, 1)
    localPlay.castShadow = true
    localPlay.receiveShadow = true
    scene.add(localPlay)

    var remotePlay = new THREE.Mesh(geometryRemote, materialRemote)
    remotePlay.position.set(0, 0, 0)
    remotePlay.castShadow = true
    remotePlay.receiveShadow = true
    scene.add(remotePlay)

    // var groundgeometry = new THREE.BoxBufferGeometry(15, 0.5, 10);
    // var material = new THREE.MeshPhongMaterial({
    //   color: 0xa0adaf,
    //   shininess: 150,
    //   specular: 0x111111,
    // });

    // var ground = new THREE.Mesh(groundgeometry, material);
    // ground.scale.multiplyScalar(3);
    // ground.castShadow = false;
    // ground.receiveShadow = true;
    // scene.add(ground);
  }

  const onClickEndCall = () => {
    console.log('end call')
    if (remoteStream != null) {
      remoteStream.getTracks().forEach((track) => track.stop())
    }
    if (localStream != null) {
      localStream.getTracks().forEach((track) => track.stop())
    }
  }
  return (
    <div className={classes.callwindow}>
      <div>
        <video
          style={{ display: 'none' }}
          id='videoLocal'
          autoPlay
          ref={refLocalVideo}
        />
      </div>
      <div>
        <video
          style={{ display: 'none' }}
          id='videoRemote'
          ref={refRemoteVideo}
        />
      </div>
      <div className={classes.videocontrol}>
        <Button
          id='btnVideo'
          className={classes.btnaction}
          style={{ backgroundColor: 'blue' }}
        >
          {/* <VideocamIcon style={{ backgroundColor: "blue" }}></VideocamIcon> */}
          VideocamIcon
        </Button>
        <Button
          id='btnAudio'
          className={classes.btnaction}
          style={{ backgroundColor: 'red' }}
        >
          <MicIcon></MicIcon>
        </Button>
        <Button className={classes.btnaction} onClick={() => onClickEndCall()}>
          <CallIcon></CallIcon>
        </Button>
      </div>
    </div>
  )
}

const useStyles = makeStyles((theme) => {
  return {
    callwindow: {},
    caller: {
      color: 'blue'
    },
    video: {
      position: 'absolute'
    },

    localVideo: {
      bottom: 0,
      right: 0,
      width: '20%',
      height: '20%',
      border: '1px'
    },

    peerVideo: {
      width: '100%',
      height: '100%'
    },
    videocontrol: {
      position: 'absolute',
      bottom: '5%',
      left: '50%',
      transform: 'translate(-50%, 0)',
      height: '200px',
      width: '500px',
      textAlign: 'center',
      opacity: 0.35,
      zIndex: 1100,

      '&:hover': {
        opacity: 1
      }
    },
    btnaction: {
      height: '60px',
      width: '60px',
      margin: '20px 30px 0px 0px',
      lineHeight: '60px',
      textAlign: 'center',
      borderRadius: '50%',
      border: 'solid 2px $main-color',
      cursor: 'pointer',
      transitionDuration: '0.25s',
      backgroundColor: '#FFFFFF',
      '&:hover': {
        backgroundColor: 'red'
      }
    },
    buttonpresenter: {
      margin: 'auto',
      width: 200
    },
    divvideo: {
      position: 'relative',
      [theme.breakpoints.up('xs')]: {
        width: 640,
        height: 480
      },
      [theme.breakpoints.down('xs')]: {
        width: 480,
        height: 320
      }
    },
    videoplay: {
      width: '90%',
      height: '100%',
      position: 'absolute'
    },
    countviewer: {
      position: 'absolute',
      left: 8,
      top: 6,
      display: 'flex',
      flexDirection: 'row'
    }
  }
})
export default CallWindow
