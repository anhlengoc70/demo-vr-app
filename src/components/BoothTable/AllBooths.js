import React, { useEffect } from "react";
import {
  Button,
  AppBar,
  Toolbar,
  Typography,
  makeStyles,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Grid,
  TextField,
} from "@material-ui/core";
import allbooths from "./allbooth.json";

export default function AllBooth() {
  const classes = useStyles();
  return (
    <div
      style={{
        width: "95%",
        margin: "0 auto",
        position: "relative",
      }}
    >
      <table style={{ width: "100%", margin: "0px 0 0 20px" }}>
        <tbody>
          <tr style={{ display: "flex", flexDirection: "row" }}>
            <td style={{ width: "33%" }}>
              {allbooths?.map((booth, index) => {
                return (
                  <div>
                    <p style={{ fontSize: "0.7rem" }}>
                      <a href={booth.href}>{booth.title}</a>
                    </p>
                  </div>
                );
              })}
            </td>
            <td style={{ width: "33%" }}>
              {allbooths?.map((booth, index) => {
                return (
                  <p style={{ fontSize: "0.7rem" }}>
                    <a href={booth.href}>{booth.title}</a>
                  </p>
                );
              })}
            </td>
            <td style={{ width: "33%" }}>
              {allbooths?.map((booth, index) => {
                return (
                  <p style={{ fontSize: "0.7rem" }}>
                    <a href={booth.href}>{booth.title}</a>
                  </p>
                );
              })}
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    height: "100vh",
  },
  form: {
    [theme.breakpoints.down("sm")]: {
      width: "100%",
    },
    [theme.breakpoints.up("md")]: {
      width: "78%",
    },
    [theme.breakpoints.up("lg")]: {
      width: "64%",
    },
    height: "80vh",
    margin: "auto",
  },
  appBar: {
    borderBottom: `1px solid ${theme.palette.divider}`,
    height: "7%",
  },
  toolbar: {
    display: "flex",
  },
  toolbarTitle: {
    flexGrow: 1,
    [theme.breakpoints.down("sm")]: {
      maxWidth: "70%",
    },
    [theme.breakpoints.up("md")]: {
      maxWidth: "25%",
    },
    [theme.breakpoints.up("lg")]: {
      maxWidth: "15%",
    },
  },
  link: {
    margin: theme.spacing(1, 1.5),
  },
  heroContent: {
    padding: theme.spacing(8, 0, 6),
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));
