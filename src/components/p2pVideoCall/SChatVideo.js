import React, { useRef, useEffect, useState } from "react";
//import Peer from "peerjs";
import { makeStyles } from "@material-ui/core";
import * as qs from "query-string";
import styles from "./Home.module.css";
import Peer from "skyway-js";

let localStream = null;
let currentCall = null;
let remoteStream = {};
let peerConnection = {};
let callConnection = {};

function SChatVideo() {
  let peer = null;
  const refLocalVideo = useRef();
  const refRemoteVideo = useRef();
  const [listCallVideo, setListCallVideo] = useState([]);
  const [statusCall, setStatusCall] = useState("Please select viewer to talk");
  const classes = useStyles();

  const handleCallClick = (peerid) => {
    refRemoteVideo.current.srcObject = null;
    setStatusCall("You are answering to : " + peerid);
    if (remoteStream[peerid]) {
      refRemoteVideo.current.srcObject = remoteStream[peerid];
      setStatusCall("You are talking to : " + peerid);
      peerConnection[peerid].send("on");
    } else {
      callConnection[peerid].answer(localStream);
    }
    if (currentCall)
      if (currentCall?.peer != peerid)
        peerConnection[currentCall.peer].send("off");
    currentCall = callConnection[peerid];
  };

  useEffect(() => {
    var query = qs.parse(window.location.search);
    console.log(query);
    if (query) {
      navigator.mediaDevices
        .getUserMedia({ video: true, audio: true })
        .then(function (mediaStream) {
          localStream = mediaStream;
          refLocalVideo.current.srcObject = localStream;

          peer = new Peer(query.streamer + "",{key: "e55b0dee-e162-4f9d-9082-e3f045a0220b",debug: 3});
          peer.on("open", function (id) {
            peer.on("connection", function (conn) {
              peerConnection[conn.peer] = conn;
              conn.on("open", function () {
                conn.on("data", function (data) {
                  console.log("Received", data);
                });

                // conn.send('Hello!');
              });
            });

            peer.on("call", function (call) {
              callConnection[call.peer] = call;
              if (!listCallVideo.includes(call.peer)) {
                listCallVideo.push(call.peer);
                setListCallVideo([...listCallVideo]);
                console.log(listCallVideo);
              } else {
                remoteStream[call.peer] = null;
              }
              call.on("stream", function (stream) {
                refRemoteVideo.current.srcObject = stream;
                setStatusCall("You are talking to : " + call.peer);
                remoteStream[call.peer] = stream;
              });
            });
          });
        });
    }
  }, []);

  return (
        <div className={classes.MainContainer}>
          <div className={classes.ListCallContainer}>
            <h3>List Calling</h3>
            {listCallVideo.map((peerid) => {
              return (
                <li
                  style={{ cursor: "pointer", margin: "4px" }}
                  onClick={() => handleCallClick(peerid)}
                >
                  {peerid}
                </li>
              );
            })}
          </div>
          <div style = {{width: "40%"}}>
            <h3 style={{ textAlign: "center" }}>{statusCall}</h3>
            <div className={classes.PChatContainer}>
              <div className={classes.RChatWrapper}>
                <div className={classes.RVideoWrapper}>
                  <video
                    className={classes.remotevideo}
                    ref={refRemoteVideo}
                    id="webcamvideo"
                    autoPlay
                    muted
                  ></video>
                </div>
              </div>
              <div className={classes.LChatWrapper}>
                <div className={classes.LVideoWrapper}>
                  <video
                    className={classes.localvideo}
                    ref={refLocalVideo}
                    id="webcamvideo"
                    autoPlay
                    muted
                  ></video>
                </div>
              </div>
            </div>
          </div>
          <footer className={styles.footer}>Powered by skyACE</footer>
        </div>
  );
}
const useStyles = makeStyles((theme) => ({
  ListCallContainer: {
    display: "flex",
    flexDirection: "column",
    minHeight: "400px",
    width: "20%",
    marginLeft: "10%"
    // background: black,
  },
  PChatContainer: {
    //display: "flex",
    //flexDirection: "column",
    position: "relative",
    minHeight: "480px",
    // background: black,
  },
  LVideoWrapper: {
    display: "flex",
    width: "100%",
    background: "black",
  },
  localvideo: {
    width: "100%",
    height: "100%",
  },
  RVideoWrapper: {
    display: "flex",
    width: "100%",
    height: "480px",
    background: "black",
  },
  remotevideo: {
    width: "100%",
    height: "100%",
  },
  LChatWrapper: {
    display: "flex",
    flexDirection: "column",
    width: " 240px",
    minHeight: "260px",
    position: "absolute",
    right: "0px",
    bottom: "0px",
  },
  RChatWrapper: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    zIndex: 1,
  },
  MainContainer: {
    display: "flex",
    flexDirection: "row",
    minHeight: "480px",
    width: window.innerWidth,
    // background: black,
  },
}));
export default SChatVideo;
