import React, { useRef, useEffect, useState } from 'react';
//import Peer from 'peerjs';
import { makeStyles } from "@material-ui/core";
import Peer from "skyway-js";


let localStream = null;
let remoteStream = null;

function VChatVideo({viewer, streamer}) {
  let peer = null;
  const refLocalVideo = useRef();
  const refRemoteVideo = useRef();
  const [statusCall, setStatusCall] = useState('Wating for streamer answer ...');
  const classes = useStyles();

  useEffect(() => {
    console.log("vvvv", viewer, streamer)
    if (viewer != "") {
      navigator.mediaDevices
        .getUserMedia({ video: true, audio: true })
        .then(function (mediaStream) {
          localStream = mediaStream;
          refLocalVideo.current.srcObject = localStream;

          peer = new Peer(viewer + '', {key: "e55b0dee-e162-4f9d-9082-e3f045a0220b",debug: 3});
          peer.on('open', function (id) {
            if (streamer != "") {
              let conn = peer.connect(streamer);
              conn.on('open', function () {
                conn.on('data', function (data) {
                  if (data === 'off') refRemoteVideo.current.srcObject = null;
                  if (data == 'on' && remoteStream) refRemoteVideo.current.srcObject = remoteStream;
                });
                // conn.send('Hello!');
              });

              setStatusCall('You are calling to : ' + streamer);
              var call = peer.call(streamer, localStream);
              call.on('stream', function (stream) {
                remoteStream = stream;
                refRemoteVideo.current.srcObject = remoteStream;
                setStatusCall('You are talking to : ' + streamer);
              });
            }
          });
        });
    }
  }, []);

  return (
    <div className = {classes.MainContainer}>
      <div>
        <h3 style={{ textAlign: 'center' }}>{statusCall}</h3>
        <div className = {classes.PChatContainer}>
          <div className = {classes.RChatWrapper}>
            <div className = {classes.RVideoWrapper}>
              <video
                className={classes.remotevideo}
                ref={refRemoteVideo}
                id='webcamvideo'
                autoPlay
                muted
              ></video>
            </div>
          </div>
          <div className = {classes.LChatWrapper}>
            <div className = {classes.LVideoWrapper}>
              <video
                className={classes.localvideo}
                ref={refLocalVideo}
                id='webcamvideo'
                autoPlay
                muted
              ></video>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
const useStyles = makeStyles((theme) => ({
  PChatContainer: {
    display: "flex",
    flexDirection: "column",
    position: "relative",
    width: "720px",
    minHeight: "480px",
    // background: black,
  },
  LVideoWrapper: {
    display: "flex",
    width: "100%",
    background: "black",
  },
  localvideo: {
    width: "100%",
    height: "100%",
  },
  RVideoWrapper: {
    display: "flex",
    width: "100%",
    height: "480px",
    background: "black",

  },
  remotevideo: {
    width: "100%",
    height: "100%",
  },
  LChatWrapper: {
  display: "flex",
  flexDirection: "column",
  width:" 260px",
  minHeight: "160px",
  position: "absolute",
  right: "0px",
  bottom: "0px",
  },
  RChatWrapper: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
  },
  MainContainer: {
    display: "flex",
    flexDirection: "row",
    width: "720px",
    margin: "10px auto",
    minHeight: "480px",
    // background: black,
    // background: black,
  },
}));
export default VChatVideo;
