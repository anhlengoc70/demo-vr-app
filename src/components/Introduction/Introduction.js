import React from 'react'
import { AppContext } from '../../context/app/app.context'
import imageIntro from '../../assets/background/GT27T0087_kiji_2.png'
export default function Introduction() {
  const {
    appState: { openIntroModal },
    appDispatch
  } = React.useContext(AppContext)

  const handleClose = () => {
    appDispatch({
      type: 'OPEN_INTRO_MODAL',
      item: {
        isOpen: false
      }
    })
  }

  return (
    <div>
      <div id='text' style={{ margin: 20 }}>
        <h1>帝人フロンティア株式会社</h1>
        <h3>出展内容</h3>
        <h5>
          ■概要・特長
          　調達が容易で可搬性に優れ、現地で組み立てが可能なパネルタンクに特殊繊維担体を設置したユニッ。
          　本技術は以下の特徴があります。
          　①高耐久かつ高負荷運転が可能で、運転管理が容易な特殊繊維担体を使用します。
          　②調達が容易で可搬性に優れたパネルタンクを用いることで、迅速な施工が可能です。
        </h5>
      </div>
      <img
        style={{ margin: 'auto' }}
        id='image'
        width='800'
        height='500'
        src={imageIntro}
      ></img>
      <button
        style={{
          position: 'absolute',
          top: 10,
          right: 10,
          backgroundColor: '#ffffff',
          color: '#222222',
          padding: '6px 8px',
          boxShadow: '0 2px 8px rgba(0,0,0,0.4)',
          opacity: 0.8,
          width: 30,
          height: 30,
          outline: 0,
          border: 0,
          cursor: 'pointer'
        }}
        onClick={() => handleClose()}
      >
        <svg
          xmlns='http://www.w3.org/2000/svg'
          width='12px'
          height='12px'
          viewBox='0 0 32 32'
        >
          <path
            id='_ionicons_svg_ios-close_5_'
            d='M179.418,175.84l10.925-10.925a2.56,2.56,0,0,0-3.62-3.62L175.8,172.22l-10.925-10.925a2.56,2.56,0,1,0-3.62,3.62l10.925,10.925-10.925,10.925a2.56,2.56,0,0,0,3.62,3.62L175.8,179.46l10.925,10.925a2.56,2.56,0,0,0,3.62-3.62Z'
            transform='translate(-160.5 -160.55)'
            fill='currentColor'
          ></path>
        </svg>
      </button>
    </div>
  )
}
