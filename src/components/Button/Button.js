import React from 'react'
import PropTypes from 'prop-types'
import { ButtonCustom } from './Button.style'

const Button = (props) => {
  const { content, size, shape, children } = props

  return (
    <ButtonCustom {...props} shape={shape ?? 'round'} size={size ?? 'large'}>
      {content || children}
    </ButtonCustom>
  )
}

Button.propTypes = {
  content: PropTypes.string,
  size: PropTypes.string,
  shape: PropTypes.string,
  children: PropTypes.any
}

export default Button
