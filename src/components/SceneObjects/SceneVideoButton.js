import React from 'react'
import { CSS3DObject } from 'three/examples/jsm/renderers/CSS3DRenderer'
import { AppContext } from '../../context/app/app.context'
import youtubeImage from '../../assets/youtube.png'
var videoData = []
const RenderVideo = ({ scene, videos }) => {
  const {
    appState: { openIntroModal, openVideoModal },
    appDispatch
  } = React.useContext(AppContext)
  React.useEffect(() => {
    render()
  }, [videos])

  const render = () => {
    videos.map((video, i) => {
      var element = document.createElement('div')
      element.id = video.title
      element.style.opacity = 0
      element.style.width = '40px'
      element.style.height = '25px'
      element.style.border = '1px solid orange'
      element.style.borderRadius = '3px'
      for (var opacity = 0; opacity <= 0.9; opacity += 0.1) {
        setTimeout(() => {
          element.style.opacity = opacity
        }, 700)
      }
      element.style.margin = 'auto'
      element.style.color = 'red'

      element.style.fontSize = '10px'
      element.style.backgroundColor = 'orange'
      element.style.transition = 'all 0.5s'

      element.style.backgroundImage = 'url(' + youtubeImage + ')'
      element.style.backgroundSize = '40px 25px'
      element.style.transition = 'all 0.5s'
      element.addEventListener('mouseover', () => onMouseOver(element))
      element.addEventListener('mouseout', () => onMouseOut(element))
      ;['click', 'touchstart'].forEach((evt) =>
        element.addEventListener(evt, () => {
          appDispatch({
            type: 'OPEN_VIDEO_MODAL',
            item: {
              isOpen: true
            }
          })
        })
      )
      var domObject = new CSS3DObject(element)
      domObject.position.set(
        video.location.posX,
        video.location.posY,
        video.location.posZ
      )
      domObject.rotateY(video.location.rotate)
      domObject.name = video.title
      scene.add(domObject)
    })
  }
  function onMouseOver(element) {
    element.style.width = 1.3 * 40 + 'px'
    element.style.backgroundSize = 1.3 * 40 + 'px 25px'
  }
  function onMouseOut(element) {
    element.style.width = 40 + 'px'
    element.style.backgroundSize = 40 + 'px 25px'
  }
  return <div id='videoButton' style={{ position: 'absolute' }}></div>
}
export default RenderVideo
