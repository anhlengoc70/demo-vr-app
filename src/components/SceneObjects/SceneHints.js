import React, { useEffect } from 'react'
import { CSS3DObject } from 'three/examples/jsm/renderers/CSS3DRenderer'
import { AppContext } from '../../context/app/app.context'

function RenderHints({ scene, points }) {
  const {
    appState: { openIntroModal },
    appDispatch
  } = React.useContext(AppContext)

  useEffect(() => {
    render()
  }, [points])
  const render = () => {
    points.map((point, i) => {
      var element = document.createElement('div')
      element.id = point.title
      element.style.width = '5px'
      element.style.height = '5px'
      element.style.border = '5px solid red'
      element.style.borderRadius = '50%'
      element.style.opacity = 0
      for (var opacity = 0; opacity <= 0.9; opacity += 0.1) {
        setTimeout(() => {
          element.style.opacity = opacity
        }, 700)
      }
      element.style.color = 'red'
      element.style.margin = 'auto'
      element.style.transition = 'all 0.5s'
      ;['click', 'touchstart'].forEach((evt) =>
        element.addEventListener(evt, () => {
          appDispatch({
            type: 'OPEN_INTRO_MODAL',
            item: {
              isOpen: true
            }
          })
        })
      )
      var domObject = new CSS3DObject(element)
      domObject.position.set(
        point.location.posX,
        point.location.posY,
        point.location.posZ
      )
      domObject.rotateY(point.location.rotate)
      domObject.name = point.title
      scene.add(domObject)
    })
  }
  return <div id='point' style={{ position: 'absolute' }}></div>
}
export default RenderHints
