import React from 'react'
import { CSS3DObject } from 'three/examples/jsm/renderers/CSS3DRenderer'
import { AppContext } from '../../context/app/app.context'
import CallUsImage from '../../assets/callus.jpg'
const RenderVideoCallButton = ({ scene, index, booth }) => {
  const {
    appState: {},
    appDispatch
  } = React.useContext(AppContext)
  React.useEffect(() => {
    render()
  }, [index])

  const render = () => {
    var element = document.createElement('div')
    element.id = 'store' + index
    element.style.opacity = 0
    element.style.width = '50px'
    element.style.height = '50px'
    element.style.border = '1px solid orange'
    element.style.borderRadius = '50%'
    for (var opacity = 0; opacity <= 0.9; opacity += 0.1) {
      setTimeout(() => {
        element.style.opacity = opacity
      }, 700)
    }
    element.style.margin = 'auto'
    element.style.color = 'red'

    element.style.fontSize = '10px'
    element.style.backgroundColor = 'orange'
    element.style.transition = 'all 0.5s'

    element.style.backgroundImage = 'url(' + CallUsImage + ')'
    element.style.backgroundSize = '50px 50px'
    element.style.transition = 'all 0.5s'
    element.addEventListener('mouseover', () => onMouseOver(element))
    element.addEventListener('mouseout', () => onMouseOut(element))
    ;['click', 'touchstart'].forEach((evt) =>
      element.addEventListener(evt, () => {
        appDispatch({
          type: 'OPEN_VIEWER_CALL_MODAL',
          item: {
            isOpenCall: true
          }
        })
        ClearObjects()
      })
    )
    var domObject = new CSS3DObject(element)
    if (index === 0) {
      domObject.position.set(250, -50, -500)
      domObject.rotateY(0)
    }
    if (index === 1) {
      domObject.position.set(-500, -50, -250)
      domObject.rotateY(Math.PI / 2)
    }
    if (index === 2) {
      domObject.position.set(-250, -50, 500)
      domObject.rotateY(-Math.PI)
    }
    if (index === 3) {
      domObject.position.set(500, -50, 250)
      domObject.rotateY(-Math.PI / 2)
    }
    domObject.name = 'store' + index
    scene.add(domObject)
  }
  function onMouseOver(element) {
    element.style.width = 1.1 * 50 + 'px'
    element.style.height = 1.1 * 50 + 'px'
    element.style.backgroundSize = 1.1 * 50 + 'px' + ' ' + 1.1 * 50 + 'px'
  }
  function onMouseOut(element) {
    element.style.width = 50 + 'px'
    element.style.height = 50 + 'px'
    element.style.backgroundSize = 50 + 'px 50px'
  }

  function ClearObjects() {
    //remove all hint, and video button in current scene before leave
    booth.stores.map((store, index) => {
      var selectedButton = scene.getObjectByName('store' + index)
      scene.remove(selectedButton)
      store.points.map((point, i) => {
        var selectedPoint = scene.getObjectByName(point.title)
        scene.remove(selectedPoint)
      })
      store.videos.map((video, i) => {
        var selectedVideo = scene.getObjectByName(video.title)
        scene.remove(selectedVideo)
      })
      var selectedObject = scene.getObjectByName(store.image)
      scene.remove(selectedObject)
    })

    booth.ways.map((way, index) => {
      var selectedWay = scene.getObjectByName(way.id)
      scene.remove(selectedWay)
    })
  }

  return <div id='VideoCallButton' style={{ position: 'absolute' }}></div>
}
export default RenderVideoCallButton
