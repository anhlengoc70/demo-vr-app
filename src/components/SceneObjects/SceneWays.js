import React, { useEffect, useState, useRef } from 'react'
import { CSS3DObject } from 'three/examples/jsm/renderers/CSS3DRenderer'
import { AppContext } from '../../context/app/app.context'
import styled, { keyframes, css } from 'styled-components'
// import { makeStyles, useTheme } from '@material-ui/core/styles'
import arrowImage from '../../assets/3d-arrow.jpg'
import audioSrc from '../../assets/media/menu-click.wav'
var opacity = 0
var intervalID = 0
const audio = new Audio(audioSrc)

function RenderWays({ scene, booth }) {
  const {
    appState: { openIntroModal, openVideoModal, wayToGo },
    appDispatch
  } = React.useContext(AppContext)
  const pointref = useRef()
  const [isDisplay, setIsDisplay] = useState(true)

  useEffect(() => {
    render()
  }, [booth])

  const render = () => {
    booth.ways.map((way, index) => {
      var element = document.createElement('div')
      element.id = way.id
      element.style.width = '50px'
      element.style.height = '20px'
      //element.style.border = "1px solid orange";
      element.style.boxShadow = '30px'
      element.style.borderRadius = '3px'
      element.style.opacity = 0
      for (var opacity = 0; opacity <= 0.9; opacity += 0.1) {
        setTimeout(() => {
          element.style.opacity = opacity
        }, 300)
      }
      element.style.margin = 'auto'
      element.style.color = 'red'

      element.style.fontSize = '10px'
      element.style.backgroundColor = 'orange'
      element.style.transition = 'all 0.5s'

      element.style.backgroundImage = 'url(' + arrowImage + ')'
      element.style.backgroundSize = '50px 20px'
      element.addEventListener('mouseover', () => onMouseOver(element))
      element.addEventListener('mouseout', () => onMouseOut(element))
      element.addEventListener('click', function () {
        setIsDisplay(false)
        //remove all door in current scene before leave
        audio.play()
        booth.ways.map((way, index) => {
          var selectedWay = scene.getObjectByName(way.id)
          scene.remove(selectedWay)
        })
        //remove all hint, and video button in current scene before leave
        booth.stores.map((store, i) => {
          store.points.map((point, i) => {
            var selectedPoint = scene.getObjectByName(point.title)
            scene.remove(selectedPoint)
          })
          store.videos.map((video, i) => {
            var selectedVideo = scene.getObjectByName(video.title)
            scene.remove(selectedVideo)
          })
          var selectedObject = scene.getObjectByName(store.image)
          scene.remove(selectedObject)
        })
        appDispatch({
          type: 'GO_TO_NEW_BOOTH',
          item: {
            wayId: way.boothTogo
          }
        })
      })
      var domObject = new CSS3DObject(element)
      domObject.position.set(
        way.location.posX,
        way.location.posY,
        way.location.posZ
      )
      domObject.rotateY(1.7 + way.location.rotate)
      domObject.name = way.id
      scene.add(domObject)
    })
  }
  function onMouseOver(element) {
    element.style.width = 1.3 * 50 + 'px'
    element.style.backgroundSize = 1.3 * 50 + 'px 20px'
  }
  function onMouseOut(element) {
    element.style.width = 50 + 'px'
    element.style.backgroundSize = 50 + 'px 20px'
  }
  return <div id='point' style={{ position: 'absolute' }}></div>
}

export default RenderWays
