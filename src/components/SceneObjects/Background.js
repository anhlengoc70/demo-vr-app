import React, { lazy, useEffect } from 'react'
import { CSS3DObject } from 'three/examples/jsm/renderers/CSS3DRenderer'

var interval
function RenderBackground({ scene, booth }) {
  useEffect(() => {
    render()
  }, [booth])
  var sides = [
    {
      url: booth.stores[0].image,
      position: [-750, 0, 0],
      rotation: [0, Math.PI / 2, 0]
    },
    {
      url: booth.stores[1].image,
      position: [750, 0, 0],
      rotation: [0, -Math.PI / 2, 0]
    },
    {
      url: booth.stores[2].image,
      position: [0, 0, 750],
      rotation: [0, Math.PI, 0]
    },
    {
      url: booth.stores[3].image,
      position: [0, 0, -750],
      rotation: [0, 0, 0]
    }
  ]
  const render = () => {
    sides.map((side, i) => {
      console.log(side + 'side')
      var element = document.createElement('img')
      element.width = 1026 // 2 pixels extra to close the gap.
      element.style.backgroundSize = '1026px 512px'
      element.src = lazy(() =>
        import(`../../assets/background/${side.url}.png`)
      )
      element.style.opacity = 0.1
      element.dataRotation = '0'
      // for (var opacity = 0.3; opacity <= 2; opacity += 0.05) {
      //   setTimeout(() => {
      //     element.style.opacity = opacity;
      //   }, 1000);
      // }
      let opacity = 0.1
      interval = setInterval(() => {
        opacity += 0.1
        element.style.opacity = opacity
        if (opacity === 1) {
          clearInterval(interval)
          console.log('clear')
        }
      }, 100)
      var object = new CSS3DObject(element)
      object.name = side.url
      object.position.fromArray(side.position)
      object.rotation.fromArray(side.rotation)
      scene.add(object)
    })
  }
  return <div id='main'></div>
}

export default RenderBackground
