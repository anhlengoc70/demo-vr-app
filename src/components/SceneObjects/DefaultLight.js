
import * as THREE from "three";

const DefaultLight = (scene) => {
    var ambientLight = new THREE.AmbientLight(0xcccccc);
    scene.add(ambientLight);
  };
  export default DefaultLight;