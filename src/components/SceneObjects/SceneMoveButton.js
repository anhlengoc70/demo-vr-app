import React from 'react'
import * as THREE from 'three'
import { CSS3DObject } from 'three/examples/jsm/renderers/CSS3DRenderer'
import { AppContext } from '../../context/app/app.context'

const RenderDirection = ({ scene, camera, booth }) => {
  const {
    appState: { openIntroModal, openVideoModal },
    appDispatch
  } = React.useContext(AppContext)
  React.useEffect(() => {
    render()
  }, [booth])

  function easeOut(t, b, c, d) {
    const ts = (t /= d) * t
    const tc = ts * t
    return b + c * (tc + -3 * ts + 3 * t)
  }

  const render = () => {
    booth.ways.map((way, i) => {
      //create SVG and style
      let svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
      svg.id = way.id
      svg.style.height = 45
      svg.style.width = 82
      svg.style.transition = 'transform 1s ease-out'

      //add polygon to SVG
      let polygonA = document.createElementNS(
        'http://www.w3.org/2000/svg',
        'polygon'
      )
      let polygonB = document.createElementNS(
        'http://www.w3.org/2000/svg',
        'polygon'
      )

      //style for polygonA
      polygonA.setAttribute('points', '40,6 0,26 1,34 40,14 79,34 80,26 40,6')
      polygonA.setAttribute('fill', 'rgba(100,100,100,15)')
      polygonA.setAttribute('stroke', 'rgba(100,100,100,15)')
      polygonA.setAttribute('stroke-width', '1')
      polygonA.style.visibility = polygonA.style.opacity = 0.8
      polygonA.style.pointerEvents = 'visiblepainted'
      polygonA.style.cursor = 'pointer'
      polygonA.style.borderRadius = 2
      polygonA.style.visibility = 'visible'

      //style for polygonB
      polygonB.setAttribute('points', '40,2 0,22 1,30 40,10 79,30 80,22 40,2')
      polygonB.setAttribute('fill', 'rgb(255,255,255)')
      polygonB.setAttribute('stroke', 'rgb(255,255,245)')
      polygonB.setAttribute('stroke-width', '1')
      polygonB.style.opacity = 1
      polygonB.style.pointerEvents = 'visiblepainted'
      polygonB.style.cursor = 'pointer'
      polygonB.style.visibility = 'visible'
      polygonB.style.borderRadius = 2
      polygonB.addEventListener('mouseover', () => onMouseOver(polygonB))
      polygonB.addEventListener('mouseout', () => onMouseOut(polygonB))

      //add EventListener user click
      polygonB.addEventListener('click', () => {
        moveToNewBooth(way)
      })

      //append polygon to SVG
      svg.appendChild(polygonA)
      svg.appendChild(polygonB)

      //Render SVG to three scene as CSS3DObject
      var domObject = new CSS3DObject(svg)
      domObject.position.set(
        way.location.posX,
        way.location.posY,
        way.location.posZ
      )
      domObject.rotateX(Math.PI * way.location.rotateX)
      domObject.rotateY(Math.PI * way.location.rotateY)
      domObject.rotateZ(Math.PI * way.location.rotateZ)
      domObject.name = way.id
      scene.add(domObject)
    })
  }

  function moveToNewBooth(way) {
    //remove all hint, and video button in current scene before leave
    booth.stores.map((store, i) => {
      store.points.map((point, i) => {
        var selectedPoint = scene.getObjectByName(point.title)
        scene.remove(selectedPoint)
      })
      store.videos.map((video, i) => {
        var selectedVideo = scene.getObjectByName(video.title)
        scene.remove(selectedVideo)
      })
    })
    var selectedWay = scene.getObjectByName(way.id)
    selectedWay.translateY(30)
    appDispatch({
      type: 'ZOOM_IN',
      item: {
        zoomvalue: true
      }
    })
    appDispatch({
      type: 'AUDIO_PLAY',
      item: {}
    })
    setTimeout(() => {
      //remove all door in current scene before leave
      booth.ways.map((way, index) => {
        var selectedWay = scene.getObjectByName(way.id)
        scene.remove(selectedWay)
      })
      booth.stores.map((store, i) => {
        var selectedObject = scene.getObjectByName(store.image)
        scene.remove(selectedObject)
      })
      appDispatch({
        type: 'GO_TO_NEW_BOOTH',
        item: {
          wayId: way.boothTogo
        }
      })
      appDispatch({
        type: 'ZOOM_IN',
        item: {
          zoomvalue: false
        }
      })
    }, 1000)
  }
  function onMouseOver(element) {
    element.style.fill = 'rgb(215,225,245)'
    element.style.stroke = 'rgb(215,225,245)'
  }
  function onMouseOut(element) {
    element.style.fill = 'rgb(255,255,255)'
    element.style.stroke = 'rgb(255,255,245)'
  }
  return <div id='directionButton'></div>
}
export default RenderDirection
