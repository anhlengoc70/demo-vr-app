import React, {
  useEffect,
  useRef,
  forwardRef,
  useImperativeHandle
} from 'react'
import positions from '../miniMapPosition.json'
import { AppContext } from '../redux/contexts/app.context'
import imageMap from '../assets/map.png'
import pinImage from '../assets/pin_red.png'

var mpCanvas

export default function MiniMap({ rotateY, rotateArrow }) {
  const refCanvas = useRef()
  useEffect(() => {
    mpCanvas = document.getElementById('refCanvas')
    drawCanvas(rotateY, rotateArrow)
  }, [])

  const {
    appState: { wayToGo },
    appDispatch
  } = React.useContext(AppContext)

  const drawCanvas = (rotateY, rotateArrow) => {
    mpCanvas.style.opacity = 0.7
    var arc = (2 * Math.PI) / 6
    var ctx = mpCanvas.getContext('2d')
    ctx.clearRect(0, 0, mpCanvas.width, mpCanvas.height)
    ctx.fillStyle = '#e91e63'
    ctx.strokeStyle = 'red'
    ctx.beginPath()
    ctx.lineTo(20, 20)
    ctx.arc(
      20,
      20,
      15,
      (rotateArrow ? -rotateY - 0.35 * Math.PI : rotateY - 0.35 * Math.PI) *
        arc,
      (rotateArrow ? -rotateY - 0.35 * Math.PI : rotateY - 0.35 * Math.PI) *
        arc +
        arc
    )
    ctx.lineTo(20, 20)
    ctx.stroke()
    ctx.fill()
    ctx.save()
    ctx.restore()
  }

  return (
    <div
      id='floorPlan'
      style={{
        position: 'absolute',
        bottom: 2,
        left: 1,
        zIndex: 1000,
        backgroundColor: '#fffee6',
        border: '5px solid grey',
        borderRadius: 10,
        opacity: 1
      }}
    >
      <div
        id='floorplanContent'
        style={{
          top: 0,
          height: 118,
          width: 238,
          position: 'relative',
          overflow: 'hidden'
        }}
      >
        <div
          id='floorplanItem'
          style={{
            position: 'absolute',
            bottom: 0,
            left: 0,
            display: 'block'
          }}
        >
          <img
            style={{
              width: 236,
              height: 118,
              pointerEvents: 'none'
            }}
            height={240}
            width={120}
            src={imageMap}
          ></img>
          {positions.mapPoints.map((point, index) => {
            return (
              <div
                id={'mp' + point.id}
                style={{
                  width: '10px',
                  position: 'absolute',
                  textAlign: 'center',
                  pointerEvents: 'none',
                  width: 10,
                  top: point.position.top,
                  left: point.position.left,
                  animation:
                    '0.5s ease 0s infinite alternate none running pulsate',
                  position: 'absolute',
                  zIndex: 1003
                }}
              >
                <p style={{ height: '15px', width: '10px' }}>
                  <img src={pinImage} width={10} height={15}></img>
                </p>
              </div>
            )
          })}
          <div
            id={
              'mp' +
              positions.mapRadaPoints.find(
                (radaPoint) => radaPoint.store === wayToGo
              ).id +
              '_radar'
            }
            style={{
              top: positions.mapRadaPoints.find(
                (radaPoint) => radaPoint.store === wayToGo
              ).position.top,
              left: positions.mapRadaPoints.find(
                (radaPoint) => radaPoint.store === wayToGo
              ).position.left,
              display: 'block',
              position: 'absolute',
              zIndex: 1005
            }}
          >
            <canvas id='refCanvas' height={40} width={40}></canvas>
          </div>
        </div>
      </div>
    </div>
  )
}
