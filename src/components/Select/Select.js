import React from 'react'
import { SelectWrapper, SelectCustom } from './Select.style'
import PropTypes from 'prop-types'

const Select = (props) => {
  const { className, placeholder, loading, size } = props

  return (
    <SelectWrapper className={className}>
      <SelectCustom
        {...props}
        placeholder={placeholder}
        loading={loading ?? false}
        size={size ?? 'large'}
        shape={'round'}
      />
    </SelectWrapper>
  )
}

Select.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  placeholder: PropTypes.string,
  size: PropTypes.string,
  loading: PropTypes.bool
}

export default Select
