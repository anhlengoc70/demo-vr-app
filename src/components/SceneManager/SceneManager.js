import React, { useEffect, useState, useRef } from 'react'
import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import { CSS3DRenderer } from 'three/examples/jsm/renderers/CSS3DRenderer'
import Stats from 'three/examples/jsm/libs/stats.module'
import VideoPlayer from './VideoPlayer'
import Introduction from './Introduction'
import TopBar from '../TopBar'
import { Button, makeStyles } from '@material-ui/core'
import Modal from 'react-awesome-modal'
import config from '../manageBooth.json'
import positions from '../miniMapPosition.json'
import RenderBackground from '../SceneObjects/Background'
import RenderHints from '../SceneObjects/SceneHints'
import RenderVideo from '../SceneObjects/SceneVideoButton'
import RenderWays from '../SceneObjects/SceneWays'
import DefaultLight from '../SceneObjects/DefaultLight'
import RenderDirection from '../SceneObjects/SceneMoveButton'
import { AppContext } from '../redux/contexts/app.context'
import * as qs from 'query-string'
import TWEEN from '@tweenjs/tween.js'
import RenderVideoCallButton from '../SceneObjects/CallButtons'
import VChatVideo from '../p2pVideoCall/VChatVideo'
import CallModal from './video-call/CallModal'
import CallWindow from './video-call/CallWindow'
import bgHome from '../../assets/s2_final_30p.jpg'
import ClickImage from '../../assets/img/click2play.png'
import mapImage from '../assets/map.png'

var camera,
  scene = new THREE.Scene(),
  renderer,
  cssrenderer,
  controls,
  windowHalfX,
  windowHalfY,
  stats
var mouseX, mouseY
var mpCanvas
var interval

var rotateY = ''
var backRefCurrent
function SceneManager() {
  const [currentBooth, setCurrentBooth] = useState(config.booths[0])
  const [currentBoothId, setCurrentBoothId] = useState(config.initialBoothId)
  const [mapPoint, setMapPoint] = useState(positions.mapPoints[0])
  const [mapPointRadar, setMapPointRadar] = useState(positions.mapRadaPoints[0])

  const backRef = useRef()
  const classes = useStyles()
  const {
    appState: {
      openIntroModal,
      openVideoModal,
      wayToGo,
      zoomvalue,
      openViewerModal
    },
    appDispatch
  } = React.useContext(AppContext)
  windowHalfX = window.innerWidth / 2
  windowHalfY = window.innerHeight / 2
  const CAMERA_DEFAULT_FOV = 55 // Default camera FOV (Field of View)
  let scale = 0.5 // Default camera Scale
  const clock = new THREE.Clock()

  //Get Width and Height of canvas contain
  const screenDimensions = {
    width: window.innerWidth,
    height: window.innerHeight
  }

  function buildScene() {
    // Load the background texture
    var textureback = new THREE.TextureLoader().load(bgHome, () => {
      const bg = new THREE.WebGLCubeRenderTarget(textureback.image.height)
      bg.fromEquirectangularTexture(renderer, textureback)
      scene.background = bg
    })
  }
  // create background renderer
  function buildRender({ width, height }) {
    const renderer = new THREE.WebGLRenderer({
      antialias: true
    })
    const DPR = window.devicePixelRatio ? window.devicePixelRatio : 1
    renderer.setPixelRatio(DPR)
    renderer.setSize(width, height)

    return renderer
  }
  // create css object renderer
  function buildCssObjectRender({ width, height }) {
    const cssrenderer = new CSS3DRenderer()
    cssrenderer.setSize(width, height, false)
    cssrenderer.domElement.style.position = 'absolute'
    cssrenderer.domElement.style.top = '0'
    return cssrenderer
  }

  // create Perspective Camera
  function buildCamera({ width, height }) {
    const aspectRatio = width / height
    const fieldOfView = CAMERA_DEFAULT_FOV
    const nearPlane = 1
    const farPlane = 100
    const camera = new THREE.PerspectiveCamera(
      fieldOfView,
      aspectRatio,
      nearPlane,
      farPlane
    )
    camera.position.set(0, 0, 10)
    return camera
  }
  // add and modify objects controls
  function buildControls() {
    const controls = new OrbitControls(camera, cssrenderer.domElement) // declare Orbit Control
    controls.enableZoom = false
    controls.zoomSpeed = 10
    controls.enablePan = false
    controls.enableDamping = true
    controls.rotateSpeed = -0.3
    controls.keyPanSpeed = 4
    //controls.minPolarAngle = Math.PI / 2 - 0.1; // radians
    //controls.maxPolarAngle = Math.PI / 2 + 0.1;
    return controls
  }
  //screen resize function
  function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight
    renderer.setSize(window.innerWidth, window.innerHeight)
    camera.updateProjectionMatrix()
  }
  function onDocumentMouseMove(event) {
    mouseX = event.clientX - windowHalfX
    mouseY = event.clientY - windowHalfY
  }

  // initial state
  useEffect(() => {
    backRefCurrent = backRef.current
    mpCanvas = document.getElementById('refCanvas')
    buildScene() // Build scene
    drawCanvas(0)
    camera = buildCamera(screenDimensions) // Build PerspectiveCamera Camera
    console.log('fov', camera.fov)
    renderer = buildRender(screenDimensions) // Build renderer for DomElement
    window.addEventListener('resize', onWindowResize, false)
    backRefCurrent.appendChild(renderer.domElement)
    stats = new Stats()
    stats.domElement.style.cssText =
      'position:absolute;bottom:10px;right:10px;z-index:1010'
    backRefCurrent.appendChild(stats.dom)
  }, [])

  useEffect(() => {
    backRefCurrent = backRef.current
    cssrenderer = buildCssObjectRender(screenDimensions) // Build renderer for DomElement
    backRefCurrent.appendChild(cssrenderer.domElement)
    controls = buildControls(screenDimensions)
    controls.addEventListener('change', function () {
      if (controls) {
        rotateY = controls.getAzimuthalAngle().toString()
        let rotateArrow = rotateY > 0 ? true : false
        console.log('canvas move', Math.abs(rotateY), rotateArrow)
        drawCanvas(Math.abs(rotateY), rotateArrow)
      }
    }) // use if there is no animation loop
    animate()
    if (wayToGo != currentBoothId) {
      camera.fov = CAMERA_DEFAULT_FOV
      //camera.lookAt(scene.position); //0,0,0
      let rotate = 1599799500
      interval = setInterval(() => {
        rotate += 0.015
        rotateY = controls.getAzimuthalAngle().toString()
        let rotateArrow = rotateY > 0 ? true : false
        console.log('scene move', Math.abs(rotateY), rotate)
        var speed = rotate
        console.log('speed', speed)
        camera.position.x = Math.cos(speed)
        camera.position.z = rotateArrow ? Math.sin(speed) : -Math.sin(speed)
        console.log(camera.position.x, camera.position.z)
        if (rotate > 1599799500 + (Math.PI * 3) / 4) {
          clearInterval(interval)
          console.log(
            'rotate',
            controls.getAzimuthalAngle(),
            Math.cos(speed),
            rotateArrow
          )
        }
      }, 15)
      camera.updateProjectionMatrix()
      setCurrentBooth(config.booths.find((booth) => booth.boothId === wayToGo))
      setCurrentBoothId(wayToGo)
      setMapPoint(positions.mapPoints.find((Point) => Point.store === wayToGo))
      setMapPointRadar(
        positions.mapRadaPoints.find((Point) => Point.store === wayToGo)
      )
    }
    //backRefCurrent.onwheel = onScrollCameraStateChange;
  }, [wayToGo])

  useEffect(() => {
    if (zoomvalue == true) {
      let ZOOM = 1
      interval = setInterval(() => {
        ZOOM -= 0.005
        camera.fov = ZOOM * CAMERA_DEFAULT_FOV
        camera.updateProjectionMatrix()
        if (ZOOM < 0.8) {
          clearInterval(interval)
        }
      }, 25)
    }
  }, [zoomvalue])

  useEffect(() => {
    if (controls) controls.addEventListener('change', requestRender, false)
  })

  const onClickBegin = () => {
    var overlay = document.getElementById('overlay')
    overlay.remove()
    camera.updateProjectionMatrix()
  }

  const onScrollCameraStateChange = (event) => {
    // Restrict scale
    scale += event.deltaY * 0.0001

    scale = Math.min(Math.max(0.1, scale), 0.6)

    camera.fov = scale * 2 * CAMERA_DEFAULT_FOV
    camera.updateProjectionMatrix()
  }

  const onClickCameraStateChange = (value) => {
    camera.fov = value * 2 * CAMERA_DEFAULT_FOV
    camera.updateProjectionMatrix()
  }

  function animate() {
    stats.begin()
    if (gLastMove + kStandbyAfter < Date.now()) {
      gRunning = false
    } else {
      gRunning = true
      setTimeout(function () {
        requestAnimationFrame(animate)
      }, 5)
    }

    controls.update(clock.getDelta())

    renderer.render(scene, camera)
    cssrenderer.render(scene, camera)

    stats.update()
    stats.end()
  }
  var gLastMove = Date.now()
  var gRunning = true
  var kStandbyAfter = 2000 // ms
  function requestRender() {
    gLastMove = Date.now()
    if (!gRunning) {
      requestAnimationFrame(animate)
    }
  }
  window.addEventListener('resize', requestRender, false)

  const drawCanvas = (rotateY, rotateArrow) => {
    mpCanvas.style.opacity = 0.65
    var arc = Math.PI / 3
    var ctx = mpCanvas.getContext('2d')
    ctx.clearRect(0, 0, mpCanvas.width, mpCanvas.height)
    ctx.fillStyle = '#e91e63'
    ctx.strokeStyle = 'red'
    ctx.beginPath()
    ctx.lineTo(20, 20)
    ctx.arc(
      20,
      20,
      15,
      (rotateArrow ? -rotateY - 0.35 * Math.PI : rotateY - 0.35 * Math.PI) *
        arc,
      (rotateArrow ? -rotateY - 0.35 * Math.PI : rotateY - 0.35 * Math.PI) *
        arc +
        arc
    )
    ctx.lineTo(20, 20)
    ctx.stroke()
    ctx.fill()
    ctx.save()
    ctx.restore()
  }

  return (
    <div id='root'>
      <div
        ref={backRef}
        style={{
          width: window.innerWidth,
          height: window.innerHeight
        }}
      ></div>
      {/* <div>
        <RenderBackground scene={scene} booth={currentBooth} />
        <RenderDirection scene={scene} camera={camera} booth={currentBooth} />
        {currentBooth.stores.map((store, index) => {
          return (
            <>
              <RenderHints scene={scene} points={store.points} />
              <RenderVideo scene={scene} videos={store.videos} />
              <RenderVideoCallButton
                scene={scene}
                index={index}
                booth={currentBooth}
              ></RenderVideoCallButton>
            </>
          );
        })}
      </div> */}
      {openViewerModal && (
        <CallWindow scene={scene} streamer={'12345'}></CallWindow>
      )}
      <TopBar></TopBar>
      <Modal
        onClickAway={() =>
          appDispatch({
            type: 'OPEN_VIDEO_MODAL',
            item: {
              isOpen: false
            }
          })
        }
        visible={openVideoModal}
        effect='fadeInUp'
        width='44%'
      >
        <VideoPlayer />
      </Modal>
      <Modal
        onClickAway={() =>
          appDispatch({
            type: 'OPEN_INTRO_MODAL',
            item: {
              isOpen: false
            }
          })
        }
        visible={openIntroModal}
        effect='fadeInUp'
        width='80%'
      >
        <Introduction />
      </Modal>
      {/* <Modal
          onClickAway={() =>
            appDispatch({
              type: "OPEN_VIEWER_CALL_MODAL",
              item: {
                isOpen: false,
              },
            })
          }
          visible={openViewerModal}
          effect="fadeInUp"
          width="60%"
        >
          <VChatVideo
            viewer={Math.PI.toString(36).substr(2, 9)}
            streamer={"12345"}
          />
        </Modal> */}
      <div
        id='overlay'
        style={{
          position: 'absolute',
          top: 0,
          left: 0,
          zIndex: 1100,
          width: window.innerWidth,
          height: window.innerHeight,
          backgroundColor: 'gray',
          opacity: 0.8,
          justifyContent: 'center'
        }}
      >
        <Button
          style={{
            width: 240,
            height: 200,
            background: 'url(' + ClickImage + ')',
            backgroundSize: '240px 200px',
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            opacity: 1
          }}
          id='startButton'
          onClick={onClickBegin}
        ></Button>
      </div>
      {/* <input
        type="range"
        defaultValue=".35"
        min="0.1"
        max="0.7"
        step="0.001"
        id="input"
        name="range"
        style={{
          position: "absolute",
          zIndex: 1020,
          bottom: 30,
          right: 100,
        }}
        onChange={(event) => onClickCameraStateChange(event.target.value)}
      ></input> */}
      <div
        id='floorPlan'
        style={{
          position: 'absolute',
          bottom: 2,
          left: 1,
          zIndex: 1000,
          backgroundColor: '#fffee6',
          border: '5px solid grey',
          borderRadius: 10,
          opacity: 1
        }}
      >
        <div
          id='floorplanContent'
          style={{
            top: 0,
            height: 118,
            width: 238,
            position: 'relative',
            overflow: 'hidden'
          }}
        >
          <div
            id='floorplanItem'
            style={{
              position: 'absolute',
              bottom: 0,
              left: 0,
              display: 'block'
            }}
          >
            <img
              style={{
                width: 236,
                height: 118,
                pointerEvents: 'none'
              }}
              height={240}
              width={120}
              src={mapImage}
            ></img>
            <div
              id={'mp' + mapPoint.id}
              style={{
                width: '10px',
                height: '10px',
                borderRadius: '50%',
                position: 'absolute',
                textAlign: 'center',
                pointerEvents: 'none',
                width: 10,
                backgroundColor: 'red',
                animation:
                  '0.5s ease 0s infinite alternate none running pulsate',
                zIndex: 1010,
                top: mapPoint.position.top + 2.5,
                left: mapPoint.position.left
              }}
            ></div>

            <div
              id={'mp' + mapPointRadar.id + '_radar'}
              style={{
                top: mapPointRadar.position.top,
                left: mapPointRadar.position.left,
                display: 'block',
                position: 'absolute',
                zIndex: 1005
              }}
              // onClick = {
              //   appDispatch({
              //     type: "GO_TO_NEW_BOOTH_MAP",
              //     item: {
              //       wayId: mapPointRadar.id,
              //     },
              //   })
              // }
            >
              <canvas id='refCanvas' height={40} width={40}></canvas>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    height: '100vh'
  },
  form: {
    [theme.breakpoints.down('sm')]: {
      width: '100%'
    },
    [theme.breakpoints.up('md')]: {
      width: '78%'
    },
    [theme.breakpoints.up('lg')]: {
      width: '64%'
    },
    height: '80vh',
    margin: 'auto'
  },
  appBar: {
    borderBottom: `1px solid ${theme.palette.divider}`,
    height: '7%'
  },
  toolbar: {
    display: 'flex'
  },
  toolbarTitle: {
    flexGrow: 1,
    [theme.breakpoints.down('sm')]: {
      maxWidth: '70%'
    },
    [theme.breakpoints.up('md')]: {
      maxWidth: '25%'
    },
    [theme.breakpoints.up('lg')]: {
      maxWidth: '15%'
    }
  },
  link: {
    margin: theme.spacing(1, 1.5)
  },
  heroContent: {
    padding: theme.spacing(8, 0, 6)
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  }
}))
export default SceneManager
