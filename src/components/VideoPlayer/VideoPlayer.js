import React, { useEffect } from 'react'
import { AppContext } from '../../context/app/app.context'
import YouTube from 'react-youtube'
export default function VideoPlayer({ url }) {
  const {
    appState: { openVideoModal },
    appDispatch
  } = React.useContext(AppContext)

  const handleClose = () => {
    appDispatch({
      type: 'OPEN_VIDEO_MODAL',
      item: {
        isOpen: false
      }
    })
  }
  return (
    <div>
      {/* <video
        id="video"
        crossOrigin="anonymous"
        playsInline
        autoPlay
        controls
        src={"https://media.giphy.com/media/XyIveZZnnuNwksEkKm/source.mp4"}
        style={{ width: "100%" }}
      ></video> */}
      <YouTube
        onPlay={openVideoModal}
        videoId='JpFeT82CRfQ'
        style={{ alignSelf: 'stretch', width: '100%' }}
        apiKey='519810911198-3vhb49ai09l8rq1ldjkssdldta51b3f4.apps.googleusercontent.com'
      />
      <button
        style={{
          position: 'absolute',
          top: 10,
          right: 10,
          backgroundColor: '#ffffff',
          color: '#222222',
          padding: '6px 8px',
          boxShadow: '0 2px 8px rgba(0,0,0,0.4)',
          opacity: 0.8,
          width: 30,
          height: 30,
          outline: 0,
          border: 0,
          cursor: 'pointer'
        }}
        onClick={() => handleClose()}
      >
        <svg
          xmlns='http://www.w3.org/2000/svg'
          width='12px'
          height='12px'
          viewBox='0 0 32 32'
        >
          <path
            id='_ionicons_svg_ios-close_5_'
            d='M179.418,175.84l10.925-10.925a2.56,2.56,0,0,0-3.62-3.62L175.8,172.22l-10.925-10.925a2.56,2.56,0,1,0-3.62,3.62l10.925,10.925-10.925,10.925a2.56,2.56,0,0,0,3.62,3.62L175.8,179.46l10.925,10.925a2.56,2.56,0,0,0,3.62-3.62Z'
            transform='translate(-160.5 -160.55)'
            fill='currentColor'
          ></path>
        </svg>
      </button>
    </div>
  )
}
