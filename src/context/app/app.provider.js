import React, { useReducer } from 'react'
import { AppContext } from './app.context'
import PropTypes from 'prop-types'
import audioSrc from '../../assets/media/menu-click.wav'
const INITIAL_STATE = {
  openVideoModal: false,
  openIntroModal: false,
  openViewerModal: false,
  openStreameroModal: false,
  viewerId: '',
  streamerId: '',
  openModal: false,
  wayToGo: 'A',
  wayToGoMap: 'A',
  zoomvalue: false
}
const audio = new Audio(audioSrc)
function reducer(state, action) {
  switch (action.type) {
    case 'OPEN_INTRO_MODAL':
      return {
        ...state,
        openIntroModal: action.item.isOpen
      }
    case 'OPEN_VIDEO_MODAL':
      return {
        ...state,
        openVideoModal: action.item.isOpen
      }
    case 'OPEN_VIEWER_CALL_MODAL':
      return {
        ...state,
        openViewerModal: action.item.isOpenCall
      }
    case 'GO_TO_NEW_BOOTH':
      return {
        ...state,
        wayToGo: action.item.wayId
      }
    case 'GO_TO_NEW_BOOTH_MAP':
      return {
        ...state,
        wayToGoMap: action.item.wayId
      }
    case 'AUDIO_PLAY':
      audio.play()
      return {
        ...state
      }
    case 'OPEN_ALL_BOOTH':
      return {
        ...state,
        openModal: action.item.isOpen
      }
    case 'ZOOM_IN':
      return {
        ...state,
        zoomvalue: action.item.zoomvalue
      }
    default:
      return state
  }
}

const AppProvider = ({ children }) => {
  const [appState, appDispatch] = useReducer(reducer, INITIAL_STATE)
  return (
    <AppContext.Provider value={{ appState, appDispatch }}>
      {children}
    </AppContext.Provider>
  )
}
AppProvider.propTypes = {
  children: PropTypes.any
}
export { AppProvider }
