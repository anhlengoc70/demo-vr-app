import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import {
  CSS3DObject,
  CSS3DRenderer
} from 'three/examples/jsm/renderers/CSS3DRenderer'
import Stats from 'three/examples/jsm/libs/stats.module'
import React, { useEffect } from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useHistory
} from 'react-router-dom'
import { AppContext } from '../../context/app/app.context'
import { HOME, BOOTH } from '../../constants/constants'
// import AllBooth from '../../components/AllBooths'
import bgHome from '../../assets/s1_final_30p.jpg'
import audioSrc from '../../assets/media/menu-click.wav'
var camera,
  mesh,
  renderer,
  cssrenderer,
  windowHalfX,
  windowHalfY,
  controls,
  stats
var rotateY, interval
var scene = new THREE.Scene()
var mouseX, mouseY
var backRefCurrent
const audio = new Audio(audioSrc)

export default function Home({ onClick }) {
  const history = useHistory()

  const CAMERA_DEFAULT_FOV = 80 // Default camera FOV (Field of View)
  let scale = 0.5 // Default camera Scale
  const clock = new THREE.Clock()
  const backRef = React.useRef()
  const {
    appState: { openModal },
    appDispatch
  } = React.useContext(AppContext)
  //Get Width and Height of canvas contain
  const screenDimensions = {
    width: window.innerWidth,
    height: window.innerHeight
  }

  function buildScene() {
    // Load the background texture
    var textureback = new THREE.TextureLoader().load(bgHome, () => {
      const bg = new THREE.WebGLCubeRenderTarget(textureback.image.height)
      bg.fromEquirectangularTexture(renderer, textureback)
      scene.background = bg
    })
  }
  // create background renderer
  function buildRender({ width, height }) {
    const renderer = new THREE.WebGLRenderer({
      antialias: true
    })
    const DPR = window.devicePixelRatio ? window.devicePixelRatio : 1
    console.log('window.devicePixelRatio', window.devicePixelRatio)
    renderer.setPixelRatio(DPR)
    console.log(renderer.getPixelRatio())
    renderer.setSize(width, height)

    return renderer
  }
  // create css object renderer
  function buildCssObjectRender({ width, height }) {
    const cssrenderer = new CSS3DRenderer()
    cssrenderer.setSize(width, height, false)
    cssrenderer.domElement.style.position = 'absolute'
    cssrenderer.domElement.style.top = '0'
    return cssrenderer
  }

  // create Perspective Camera
  function buildCamera({ width, height }) {
    const aspectRatio = width / height
    const fieldOfView = CAMERA_DEFAULT_FOV
    const nearPlane = 1
    const farPlane = 1000
    const camera = new THREE.PerspectiveCamera(
      fieldOfView,
      aspectRatio,
      nearPlane,
      farPlane
    )
    camera.position.set(100, -15, 0)
    camera.lookAt(scene.position)
    return camera
  }
  // add and modify objects controls
  function buildControls() {
    const controls = new OrbitControls(camera, cssrenderer.domElement) // declare Orbit Control
    controls.enableZoom = false
    controls.zoomSpeed = 10
    controls.enablePan = false
    controls.enableDamping = true
    controls.rotateSpeed = -0.3
    controls.keyPanSpeed = 4
    controls.minPolarAngle = Math.PI / 2 - 0.3 // radians
    controls.maxPolarAngle = Math.PI / 2 + 0.3
    return controls
  }
  //screen resize function
  function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight
    renderer.setSize(window.innerWidth, window.innerHeight)
    camera.updateProjectionMatrix()
  }
  function onDocumentMouseMove(event) {
    mouseX = event.clientX - windowHalfX
    mouseY = event.clientY - windowHalfY
  }

  useEffect(() => {
    backRefCurrent = backRef.current
    windowHalfX = window.innerWidth / 2
    windowHalfY = window.innerHeight / 2
    //stats = new Stats()
    cssrenderer = buildCssObjectRender(screenDimensions)
    camera = buildCamera(screenDimensions)
    buildScene(screenDimensions)
    renderer = buildRender(screenDimensions)
    controls = buildControls()
    addButtonToWeb()
    addButtonToVR()
    var geometry = new THREE.SphereBufferGeometry(750, 500, 1000)
    // invert the geometry on the x-axis so that all of the faces point inward
    geometry.scale(-1, 1, 1)

    // var loader = new THREE.TextureLoader();
    // const materialBackground = new THREE.MeshBasicMaterial({
    //   map: loader.load(require("../assets/background/exhibition.jpg")),
    // });
    // mesh = new THREE.Mesh(geometry, materialBackground);
    // mesh.name = "homepanorama";
    // scene.add(mesh);
    // backRefCurrent.appendChild(stats.dom)
    backRefCurrent.appendChild(renderer.domElement)
    backRefCurrent.appendChild(cssrenderer.domElement)

    // let rotate = 0;
    // interval = setInterval(() => {
    //   rotate += 0.015;
    //   rotateY = controls.getAzimuthalAngle().toString();
    //   let rotateArrow = rotateY > 0 ? true : false;
    //   console.log("scene move", Math.abs(rotateY), rotate);
    //   var speed = Date.now() * 0.00035;
    //   camera.position.x = Math.cos(speed);
    //   camera.position.z = -Math.sin(speed);
    //   console.log(camera.position.x, camera.position.z);
    //   if (rotate > Math.abs(rotateY)) {
    //     clearInterval(interval);
    //     console.log("rotate", controls.getAzimuthalAngle(), Math.cos(speed));
    //   }
    // }, 15);
    //camera.updateProjectionMatrix();
    animate()

    document.addEventListener('mousemove', onDocumentMouseMove, false)
    window.addEventListener('resize', onWindowResize, false)
  }, [])
  useEffect(() => {
    if (controls) controls.addEventListener('change', requestRender, false)
  })
  const buttonOnClick = (text) => {
    onClick(text)
    console.log('Start')
    audio.play()
  }
  function addButtonToVR() {
    var element = document.getElementById('vrbutton')
    element.style.width = '440px'
    element.style.height = '125px'
    element.style.border = '5px solid orange'
    element.style.borderRadius = '25px'
    element.textContent = 'GO TO VR SHOWROOM'
    element.style.fontSize = '45px'
    element.style.position = 'relative'
    element.style.justifyContent = 'center'
    element.style.textAlign = 'center'
    element.style.opacity = 1

    element.style.backgroundColor = 'green'
    element.style.transition = 'all 0.5s'
    element.addEventListener('mouseover', () => onMouseOver(element))
    element.addEventListener('mouseout', () => onMouseOut(element))
    ;['click', 'touchstart'].forEach((evt) =>
      element.addEventListener(evt, function () {
        appDispatch({
          type: 'AUDIO_PLAY',
          item: {}
        })
        appDispatch({
          type: 'OPEN_ALL_BOOTH',
          item: {
            isOpen: true
          }
        })
        history.push(BOOTH)
      })
    )

    var domObject = new CSS3DObject(element)
    domObject.position.set(-700, 75, -155)
    domObject.name = 'vrshowroom'
    domObject.rotateY(Math.PI / 2)
    scene.add(domObject)
  }

  function addButtonToWeb() {
    var element = document.getElementById('webbutton')
    element.style.width = '440px'
    element.style.height = '125px'
    element.style.border = '5px solid orange'
    element.style.borderRadius = '25px'
    element.textContent = 'GO TO VR WEB'
    element.style.fontSize = '45px'
    element.style.position = 'relative'
    element.style.justifyContent = 'center'
    element.style.textAlign = 'center'
    element.style.opacity = 1

    element.style.backgroundColor = 'grey'
    element.style.transition = 'all 0.5s'
    element.addEventListener('mouseover', () => onMouseOver(element))
    element.addEventListener('mouseout', () => onMouseOut(element))
    ;['click', 'touchstart'].forEach((evt) =>
      element.addEventListener(evt, function () {
        var selectedVR = scene.getObjectByName('vrshowroom')
        scene.remove(selectedVR)
        var selectedWeb = scene.getObjectByName('webbutton')
        scene.remove(selectedWeb)
        var selectedImg = scene.getObjectByName('homepanorama')
        scene.remove(selectedImg)
      })
    )

    var domObject = new CSS3DObject(element)
    domObject.name = 'webbutton'
    domObject.position.set(-700, -82, -155)
    domObject.rotateY(Math.PI / 2)
    scene.add(domObject)
  }
  function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight
    camera.updateProjectionMatrix()

    renderer.setSize(window.innerWidth, window.innerHeight)
  }
  function onDocumentMouseMove(event) {
    mouseX = event.clientX - windowHalfX
    mouseY = event.clientY - windowHalfY
  }

  function animate() {
    // stats.begin()
    scene.updateMatrixWorld()
    if (gLastMove + kStandbyAfter < Date.now()) {
      gRunning = false
    } else {
      gRunning = true
      setTimeout(function () {
        requestAnimationFrame(animate)
      }, 5)
    }

    controls.update(clock.getDelta())
    renderer.render(scene, camera)
    cssrenderer.render(scene, camera)

    // stats.update()
    // stats.end()
  }

  var gLastMove = Date.now()
  var gRunning = true
  var kStandbyAfter = 2000 // ms
  function requestRender() {
    gLastMove = Date.now()
    if (!gRunning) {
      requestAnimationFrame(animate)
    }
  }
  window.addEventListener('resize', requestRender, false)

  function onMouseOver(element) {
    element.style.width = 1.05 * 440 + 'px'
  }
  function onMouseOut(element) {
    element.style.width = 440 + 'px'
  }
  return (
    <div>
      <div
        style={{
          position: 'absolute',
          height: window.innerHeight,
          width: window.innerWidth
        }}
        ref={backRef}
      ></div>
      <div style={{ position: 'absolute' }} id='vrbutton'></div>
      <Link href='http://chubu-test.web-mirai.jp/mirai'>
        <div style={{ position: 'absolute' }} id='webbutton'></div>
      </Link>
    </div>
  )
}
