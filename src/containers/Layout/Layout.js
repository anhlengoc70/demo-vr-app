import React, { Fragment, useEffect } from 'react'
import PropTypes from 'prop-types'
import Header from './Header'
import Footer from './Footer'
import { MainContent } from './Layout.style'
import { Container, PageWrapper } from '../../theme/CommonStyle'
import { HOME, BOOTH } from '../../constants/constants'
import { useLocation } from 'react-router-dom'

const Layout = (props) => {
  const location = useLocation()
  const { pathname } = location

  const isVisibleHeader = pathname !== HOME
  return (
    <Fragment>
      {isVisibleHeader && <Header pathname={pathname} />}
      <MainContent>
        <Container>
          <PageWrapper>{props.children}</PageWrapper>
        </Container>
      </MainContent>
      {/* {isVisibleHeader && <Footer />} */}
    </Fragment>
  )
}

Layout.propTypes = {
  children: PropTypes.any
}

export default Layout
