import React from 'react'
import { Container } from '../../theme/CommonStyle'
import { CopyRight, FooterContent, FooterWrapper } from './Layout.style'

const Footer = () => {
  return (
    <FooterWrapper>
      <Container>
        <FooterContent>
          <CopyRight>© 2020 Sky ACE Ltd</CopyRight>
        </FooterContent>
      </Container>
    </FooterWrapper>
  )
}

export default Footer
