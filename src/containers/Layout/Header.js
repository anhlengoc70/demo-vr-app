/* eslint-disable no-undef */
import React, { memo, useContext, useState } from 'react'
import PropTypes from 'prop-types'
import queryString from 'query-string'
import imageLogo from '../../assets/Capture.png'
import Button from '../../components/Button/Button'
import SearchBox from '../../components/SearchBox/SearchBox'
import Select from '../../components/Select/Select'
import { HOME, BOOTH } from '../../constants/constants'
import { Container, MenuIcon } from '../../theme/CommonStyle'
import {
  HeaderWrapper,
  LeftWrapper,
  LogoWrapper,
  Navigation,
  RightWrapper,
  UserInfo,
  Image
} from './Layout.style'
// import { AuthContext } from '../../context/auth/auth.context'
import { useHistory, useLocation } from 'react-router-dom'
import { Confirm } from '../../helpers/Confirm'

const SelectOptions = [
  { value: 'grocery', label: 'Grocery' },
  { value: 'women-cloths', label: 'Women Cloth' },
  { value: 'bags', label: 'Bags' },
  { value: 'makeup', label: 'Makeup' }
]

const Header = (props) => {
  const { pathname } = props
  // const {
  //   authState: { isAuthenticated, role },
  //   authDispatch
  // } = useContext(AuthContext)
  const [selectValue, setSelectValue] = useState(null)

  const onSelectChange = (index) => {
    setSelectValue(index)
  }
  const history = useHistory()
  const location = useLocation()
  const search = queryString.parse(location.search)

  const searchHandler = (value) => {
    const newSearch = { ...search, searchText: value, pageIndex: 0 }
    history.push({
      search: queryString.stringify(newSearch)
    })
  }

  return (
    <HeaderWrapper>
      <Container>
        <Navigation>
          <LogoWrapper onClick={() => history.push(HOME)}>
            <Image src={imageLogo} />
          </LogoWrapper>
          {pathname === HOME && (
            <LeftWrapper>
              <Select
                options={SelectOptions}
                onChange={(value) => onSelectChange(value)}
                placeholder='Filter by'
                value={selectValue}
              />{' '}
              <Select
                options={SelectOptions}
                onChange={(value) => onSelectChange(value)}
                placeholder='Filter by'
                value={selectValue}
              />{' '}
              <SearchBox
                placeholder={'検索する'}
                onSearch={searchHandler}
                onChange={() => {}}
                defaultValue={''}
              />
            </LeftWrapper>
          )}
          <RightWrapper>
            <Button
              content={'PREVIEW'}
              variant={'secondary'}
              style={{
                fontSize: '0.8rem',
                margin: '0 1.5rem'
              }}
            />
          </RightWrapper>
        </Navigation>
      </Container>
    </HeaderWrapper>
  )
}

Header.propTypes = {
  pathname: PropTypes.string
}

export default memo(Header)
