import React from 'react'
import { CSS3DObject } from 'three/examples/jsm/renderers/CSS3DRenderer'

const RenderDirection = ({ camera, scene, makerId }) => {
  React.useEffect(() => {
    render()
  }, [])

  const render = () => {
    var element = document.getElementById('vrbutton')
    element.style.width = '450px'
    element.style.height = '125px'
    element.style.border = '5px solid blue'
    element.style.borderRadius = '25px'
    element.textContent = makerId
    element.style.display = 'flex'
    element.style.fontSize = '40px'
    element.style.alignContent = 'center'
    element.style.alignItems = 'center'
    element.style.justifyContent = 'center'
    element.style.margin = 'auto'
    element.style.paddingTop = '20px'
    element.style.textAlign = 'center'
    element.style.opacity = 1
    element.style.overflow = 'hidden'
    element.style.textOverflow = 'ellipsis'
    element.style.whiteSpace = 'nowrap'
    element.title = makerId
    element.style.backgroundColor = '#fff'
    element.style.transition = 'all 0.5s'
    element.addEventListener('mouseover', () => onMouseOver(element))
    element.addEventListener('mouseout', () => onMouseOut(element))

    var domObject = new CSS3DObject(element)
    domObject.name = 'webbutton'
    domObject.position.set(1100, 150, 0)
    domObject.rotateY(-Math.PI / 2)
    scene.add(domObject)
  }

  function onMouseOver(element) {
    element.style.fill = 'rgb(215,225,245)'
    element.style.stroke = 'rgb(215,225,245)'
  }
  function onMouseOut(element) {
    element.style.fill = 'rgb(255,255,255)'
    element.style.stroke = 'rgb(255,255,245)'
  }
  return <div id='vrbutton' />
}
export default RenderDirection
