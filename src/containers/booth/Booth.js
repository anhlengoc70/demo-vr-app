import React, { useEffect, useState, useRef } from 'react'
import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import { CSS3DRenderer } from 'three/examples/jsm/renderers/CSS3DRenderer'
import Stats from 'three/examples/jsm/libs/stats.module'
import VideoPlayer from '../../components/VideoPlayer/VideoPlayer'
import Introduction from '../../components/Introduction/Introduction'
import { Button } from 'antd'
import { Modal } from 'antd'
import config from '../../components/manageBooth.json'
import positions from '../../components/MiniMap/miniMapPosition.json'
// import RenderBackground from '../../components/SceneObjects/Background'
// import RenderHints from '../../components/SceneObjects/SceneHints'
// import RenderVideo from '../../components/SceneObjects/SceneVideoButton'
// import RenderWays from '../../components/SceneObjects/SceneWays'
// import DefaultLight from '../../components/SceneObjects/DefaultLight'
import RenderDirection from './SceneMoveButton'
import { AppContext } from '../../context/app/app.context'
import * as qs from 'query-string'
// import TWEEN from '@tweenjs/tween.js'
// import RenderVideoCallButton from '../../components/SceneObjects/CallButtons'
// import VChatVideo from '../../components/p2pVideoCall/VChatVideo'
// import CallModal from '../../components/VideoCall/CallModal'
// import CallWindow from '../../components/VideoCall/CallWindow'
import bgHome from '../../assets/s2_final_30p.jpg'
// import ClickImage from '../../assets/img/click2play.png'
// import mapImage from '../../assets/map.png'
import fontText from './helvetiker_regular.typeface.json'

import { useLocation } from 'react-router-dom'
var camera,
  scene = new THREE.Scene(),
  renderer,
  cssrenderer,
  controls,
  stats,
  cameraTarget
// windowHalfX,
// windowHalfY,
// var mouseX, mouseY
// var mpCanvas
var interval
var backRefCurrent
function SceneManager() {
  let container, stats, permalink, hex
  const location = useLocation()
  const search = qs.parse(location.search)
  const backRef = useRef()
  const {
    appState: { zoomvalue }
  } = React.useContext(AppContext)
  const CAMERA_DEFAULT_FOV = 55 // Default camera FOV (Field of View)
  // let scale = 0.5 // Default camera Scale
  const clock = new THREE.Clock()

  //Get Width and Height of canvas contain
  const screenDimensions = {
    width: window.innerWidth,
    height: window.innerHeight
  }

  function buildScene() {
    // Load the background texture
    var textureback = new THREE.TextureLoader().load(bgHome, () => {
      const bg = new THREE.WebGLCubeRenderTarget(textureback.image.height)
      bg.fromEquirectangularTexture(renderer, textureback)
      scene.background = bg
    })
  }
  // create background renderer
  function buildRender({ width, height }) {
    const renderer = new THREE.WebGLRenderer({
      antialias: true
    })
    const DPR = window.devicePixelRatio ? window.devicePixelRatio : 1
    renderer.setPixelRatio(DPR)
    renderer.setSize(width, height)

    return renderer
  }
  // create css object renderer
  function buildCssObjectRender({ width, height }) {
    const cssrenderer = new CSS3DRenderer()
    cssrenderer.setSize(width, height, false)
    cssrenderer.domElement.style.position = 'absolute'
    cssrenderer.domElement.style.top = '0'
    return cssrenderer
  }

  // create Perspective Camera
  function buildCamera({ width, height }) {
    const aspectRatio = width / height
    const fieldOfView = CAMERA_DEFAULT_FOV
    const nearPlane = 1
    const farPlane = 100
    const camera = new THREE.PerspectiveCamera(
      fieldOfView,
      aspectRatio,
      nearPlane,
      farPlane
    )
    camera.position.set(-330, 0, 0)
    return camera
  }
  // add and modify objects controls
  function buildControls() {
    const controls = new OrbitControls(camera, cssrenderer.domElement) // declare Orbit Control
    controls.enableZoom = false
    controls.zoomSpeed = 10
    controls.enablePan = false
    controls.enableDamping = true
    controls.rotateSpeed = -0.3
    controls.keyPanSpeed = 4
    //controls.minPolarAngle = Math.PI / 2 - 0.1; // radians
    //controls.maxPolarAngle = Math.PI / 2 + 0.1;
    return controls
  }
  //screen resize function
  function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight
    renderer.setSize(window.innerWidth, window.innerHeight)
    camera.updateProjectionMatrix()
  }
  // function onDocumentMouseMove(event) {
  //   mouseX = event.clientX - windowHalfX
  //   mouseY = event.clientY - windowHalfY
  // }
  // initial state
  useEffect(() => {
    backRefCurrent = backRef.current
    buildScene() // Build scene
    camera = buildCamera(screenDimensions) // Build PerspectiveCamera Camera
    renderer = buildRender(screenDimensions) // Build renderer for DomElement
    window.addEventListener('resize', onWindowResize, false)
    backRefCurrent.appendChild(renderer.domElement)
    // stats = new Stats()
    // stats.domElement.style.cssText =
    //   'position:absolute;bottom:10px;right:10px;z-index:1010'
    // backRefCurrent.appendChild(stats.dom)
  }, [])

  useEffect(() => {
    backRefCurrent = backRef.current
    cssrenderer = buildCssObjectRender(screenDimensions) // Build renderer for DomElement
    backRefCurrent.appendChild(cssrenderer.domElement)
    controls = buildControls(screenDimensions)
    animate()
    camera.updateProjectionMatrix()
    //backRefCurrent.onwheel = onScrollCameraStateChange;
    const loader = new THREE.FontLoader()
    loader.load(fontText, function (font) {
      const color = 0x006699

      const matDark = new THREE.LineBasicMaterial({
        color: color,
        side: THREE.DoubleSide
      })

      const matLite = new THREE.MeshBasicMaterial({
        color: color,
        transparent: true,
        opacity: 0.4,
        side: THREE.DoubleSide
      })
      const message = 'Three.js\nSimple text.'
      const shapes = font.generateShapes(message, 100)
      const geometry = new THREE.ShapeBufferGeometry(shapes)
      geometry.computeBoundingBox()
      const xMid =
        -0.5 * (geometry.boundingBox.max.x - geometry.boundingBox.min.x)
      geometry.translate(xMid, 0, 0)
      // make shape ( N.B. edge view not visible )
      const text = new THREE.Mesh(geometry, matLite)
      text.position.z = -150
      scene.add(text)

      // make line shape ( N.B. edge view remains visible )
      const holeShapes = []

      for (let i = 0; i < shapes.length; i++) {
        const shape = shapes[i]
        if (shape.holes && shape.holes.length > 0) {
          for (let j = 0; j < shape.holes.length; j++) {
            const hole = shape.holes[j]
            holeShapes.push(hole)
          }
        }
      }
      shapes.push.apply(shapes, holeShapes)
      const lineText = new THREE.Object3D()
      for (let i = 0; i < shapes.length; i++) {
        const shape = shapes[i]
        const points = shape.getPoints()
        const geometry = new THREE.BufferGeometry().setFromPoints(points)
        geometry.translate(xMid, 0, 0)
        const lineMesh = new THREE.Line(geometry, matDark)
        lineText.add(lineMesh)
      }
      scene.add(lineText)
    })
  }, [])

  useEffect(() => {
    if (zoomvalue === true) {
      let ZOOM = 1
      interval = setInterval(() => {
        ZOOM -= 0.005
        camera.fov = ZOOM * CAMERA_DEFAULT_FOV
        camera.updateProjectionMatrix()
        if (ZOOM < 0.8) {
          clearInterval(interval)
        }
      }, 25)
    }
  }, [zoomvalue])

  useEffect(() => {
    if (controls) controls.addEventListener('change', requestRender, false)
  })

  const onClickBegin = () => {
    var overlay = document.getElementById('overlay')
    if (overlay) overlay.remove()
    camera.updateProjectionMatrix()
  }

  // const onScrollCameraStateChange = (event) => {
  //   // Restrict scale
  //   scale += event.deltaY * 0.0001

  //   scale = Math.min(Math.max(0.1, scale), 0.6)

  //   camera.fov = scale * 2 * CAMERA_DEFAULT_FOV
  //   camera.updateProjectionMatrix()
  // }

  // const onClickCameraStateChange = (value) => {
  //   camera.fov = value * 2 * CAMERA_DEFAULT_FOV
  //   camera.updateProjectionMatrix()
  // }

  function animate() {
    // stats.begin()
    if (gLastMove + kStandbyAfter < Date.now()) {
      gRunning = false
    } else {
      gRunning = true
      setTimeout(function () {
        requestAnimationFrame(animate)
      }, 5)
    }

    controls.update(clock.getDelta())

    renderer.render(scene, camera)
    cssrenderer.render(scene, camera)

    // stats.update()
    // stats.end()
  }
  var gLastMove = Date.now()
  var gRunning = true
  var kStandbyAfter = 2000 // ms
  function requestRender() {
    gLastMove = Date.now()
    if (!gRunning) {
      requestAnimationFrame(animate)
    }
  }
  window.addEventListener('resize', requestRender, false)
  return (
    <div id='root'>
      <div
        ref={backRef}
        style={{
          width: window.innerWidth,
          height: window.innerHeight
        }}
      ></div>
      <RenderDirection
        scene={scene}
        camera={camera}
        makerId={search.maker}
        onClick={onClickBegin}
      />
      {/* <input
        type="range"
        defaultValue=".35"
        min="0.1"
        max="0.7"
        step="0.001"
        id="input"
        name="range"
        style={{
          position: "absolute",
          zIndex: 1020,
          bottom: 30,
          right: 100,
        }}
        onChange={(event) => onClickCameraStateChange(event.target.value)}
      ></input> */}
    </div>
  )
}

export default SceneManager
