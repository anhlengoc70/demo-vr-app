import React, { lazy, Suspense } from 'react'
import { Switch, Route } from 'react-router-dom'
import Layout from './containers/Layout/Layout'
// import { AuthContext } from './context/auth/auth.context'
// import { AuthProvider } from './context/auth/auth.provider'
import ErrorBoundary from 'antd/lib/alert/ErrorBoundary'
import { HOME, BOOTH } from './constants/constants'
import { AppProvider } from './context/app/app.provider'

const HomePage = lazy(() => import('./pages/HomePage'))
const Booth = lazy(() => import('./pages/BoothPage'))
function App() {
  return (
    <AppProvider>
      <Suspense fallback={<div>読み込み中...</div>}>
        <ErrorBoundary>
          <Layout>
            <Switch>
              <Route exact={true} path={HOME}>
                <Suspense fallback={<div>読み込み中...</div>}>
                  <Booth />
                </Suspense>
              </Route>
              <Route exact={true} path={BOOTH}>
                <Suspense fallback={<div>読み込み中...</div>}>
                  <HomePage />
                </Suspense>
              </Route>
            </Switch>
          </Layout>
        </ErrorBoundary>
      </Suspense>
    </AppProvider>
  )
}

export default App
