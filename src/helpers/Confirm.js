/* eslint-disable react/react-in-jsx-scope */
import React from 'react'
import { ExclamationCircleOutlined } from '@ant-design/icons'
import { Modal } from 'antd'

export const Confirm = (params) => {
  return Modal.confirm({
    ...params,
    icon: <ExclamationCircleOutlined />
  })
}
