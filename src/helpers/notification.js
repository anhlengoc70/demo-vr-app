import { notification } from 'antd'

export const Notification = (params) => {
  notification.open({
    ...params,
    placement: 'topLeft',
    top: 75
  })
}
